# Prefácio

<p>Thadeu escrevia desde muito pequeno. Acredito que ele aprendeu a pensar, ler e escrever quase simultaneamente. Tudo que ele fazia era com amor, carinho e muito questionamento, buscando sempre o novo, o desconhecido e a verdade. Assim nasceu este poeta, que aos quatro ou cinco anos já sabia um poema inteiro de có e o recitava com glamour para envaidecer a vovó Maria, sua admiradora incondicional.</p>

<p>Muitas vezes escrevia e vinha me mostrar os textos, pedindo minha opinião. Este livro foi assim, “Folhas de Outono” foi um título provisório, para inscrever na lei Rubem Braga. Estava ansioso por vê-lo publicado. Acreditava que este seria o primeiro de uma série infinita. Não teve apoio da Lei por causa de seu acidente, porém isso não foi empecilho para que seja publicado.</p>

<p>"Quac", dos meninos do Direito, escrevia compulsivamente. Ele tem poesias que não acabam mais. Acredito que todos que o conheciam tem uma poesia sua, pois escrevia em qualquer lugar, em qualquer papel, e doava quase todas. Só depois que comprou o computador que começou Transferi-las prá lá.</p>

<p>Neste livro, senti liberdade de ousar, fugir aos padrões editoriais. O livro foi dividido em três partes. A primeira é composta de textos de amigos. A segunda é o livro "Folhas de Outono" na íntegra com apresentação do Prof. Bússola. A última, uma coletânea de poesias avulsas.</p>

<p>Futuramente, vou fazer um livro de coletânea das outras poesias que não se encontram aqui, e daquelas que fez junto com amigos como
Eduardo, Guilherme, Jucimar, André, etc. Se você tiver algo a acrescentar, será bem vindo.</p>

<p>Sonhador, idealista, justo, amigo, curioso, verdadeiro, profundo, apaixonado, sem preconceitos, ousado, teimoso (às vezes), fiel (sempre), único. Assim era meu irmão, que certamente não será lembrado pelo seu livro de poesias e sim pelo seu exemplo de vida. O amor e a saudade serão eternos, até um dia…</p>


<p class="text-align-right"> 
**Máira Coelho Silva**
</p>
<p class="text-align-right"> 
23/08/97
</p>
