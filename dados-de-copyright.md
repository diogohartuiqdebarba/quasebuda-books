## Dados de Copyright

<br>

![Quasebuda](./quasebuda-logo.png)

<br>

A Quasebuda é uma editora primeiramente digital, buscando incentivar autores a publicarem seus livros sem muita dificuldade. A Quasebuda é um projeto aberto e gratuito, onde os voluntários e autores trabalham em conjunto para publicarem obras de qualidade disponibilizadas sob licença CC BY-NC-SA. Acreditamos que a informação seja livre e tudo o que criamos não criamos, não é nosso, não pertence a ninguém.

<br>

![by-nc-sa](./by-nc-sa.png)

**Atribuição-NãoComercial-CompartilhaIgual**

**CC BY-NC-SA**

Esta licença permite que outros remixem, adaptem e criem a partir do seu trabalho para fins não comerciais, desde que atribuam a você o devido crédito e que licenciem as novas criações sob termos idênticos.

<br>

---

**Título:** *Folhas de Outono*<br>
**Edição:** *Edição Digital na Quasebuda em Setembro de 2019*<br>
**Capa:** *Criada por Diogo Hartuiq Debarba utilizando a imagem de 8926 por Pixabay.*<br>
**Revisão:** *Diogo Hartuiq Debarba*<br>
**Observações:** *Livro construído a partir da 1º Edição impressa de agosto de 1997 sob os cuidados de Máira Coelho Silva.*<br>

