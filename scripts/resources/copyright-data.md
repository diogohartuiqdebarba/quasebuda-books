# Dados de Copyright

test1
test2
<br>
test3
<br>
<br>
test4

![Quasebuda](./resources/quasebuda-logo.png)

A Quasebuda é uma editora primeiramente digital, buscando incentivar autores a publicarem seus livros sem muita
dificuldade. A Quasebuda é um projeto aberto e gratuito, onde os voluntários e autores trabalham em conjunto para
publicarem obras de qualidade disponibilizadas sob licença [CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.pt). Acreditamos que a informação seja livre e tudo o
que criamos não criamos, não é nosso, não pertence a ninguém.



![License BY-NC-SA](./resources/by-nc-sa.png)

**Atribuição-NãoComercial-CompartilhaIgual**



**CC BY-NC-SA**


Esta licença permite que outros remixem, adaptem e criem a partir do seu trabalho para fins não comerciais, desde que atribuam a você o devido crédito e que licenciem as novas criações sob termos idênticos.



### Contribuições

**Título:** *${BookTitle}*

**Edição:** *Edição Digital na Quasebuda em ${month} de ${year}*

**Capa:** *Criada por ${AuthorName}*

**Revisão:** *${AuthorName}*

**Observações:** *${Notes}*
