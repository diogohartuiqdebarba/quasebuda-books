#!/bin/bash 
# ----------------------------------------------------------
# Epub and PDF Creator
# ----------------------------------------------------------
# Dependencies:
#     pandoc, texlive-bin, texlive-XeTeX and textlive-most
# ----------------------------------------------------------
# by Quasebuda - quasebuda.com
# ----------------------------------------------------------

while [[ "$#" -gt 0 ]]
do
    case $1 in
	-p|--path)
            bookPath="$2"
            ;;
    esac
    shift
done

pandoc=~/Programs/pandoc-2.11.0.2/bin/pandoc
resourcesPath=./resources
copyrightPath=./resources/copyright-data.md
bookInfoPath="${bookPath}/info.txt"
book=$(head -n 1 "${bookInfoPath}")
author=$(head -n 2 "${bookInfoPath}" | tail -1)
date=$(date '+%Y-%m-%d')
year=$(date '+%Y')

mkdir -p .tmp .tmp/epub .tmp/pdf

IFS=$'\n'
mdFiles=$(find "${bookPath}/docs" -type f -name '*.md' -print | sort)

titlePath=.tmp/epub/title.txt
echo "% ${book}" > $titlePath
echo "% ${author}" >> $titlePath
metadataPath=.tmp/epub/metadata.xml
echo "<dc:title>${book}</dc:title>" > $metadataPath
echo "<dc:language>pt-BR</dc:language>" >> $metadataPath
echo "<dc:creator opf:file-as=${author} opf:role=\"aut\">${author}</dc:creator>" >> $metadataPath
echo "<dc:publisher>quasebuda.com</dc:publisher>" >> $metadataPath
echo "<dc:date opf:event=\"publication\">${date}</dc:date>" >> $metadataPath
echo "<dc:rights>Copyright © ${year} Quasebuda</dc:rights>" >> $metadataPath

$pandoc -o "${bookPath}/build/${book} - ${author}--Quasebuda.epub" \
	--resource-path=.:"${bookPath}/assets/":$resourcesPath \
	$titlePath $copyrightPath $mdFiles \
	--epub-cover-image="${bookPath}/assets/cover.png" \
	--epub-metadata=$metadataPath \
	--toc --toc-depth=2 \
	--css="${resourcesPath}/epub.css" \
	-f markdown-implicit_figures+hard_line_breaks \
	--lua-filter="${resourcesPath}/line-breaks.lua"

	
coverPath=.tmp/pdf/cover.tex
echo "\begin{center}" > $coverPath
echo "\graphicspath{{${bookPath}/assets/}}" >> $coverPath
echo "\includegraphics{cover.png}" >> $coverPath
echo "\thispagestyle{empty}" >> $coverPath
echo "\end{center}" >> $coverPath

$pandoc -o "${bookPath}/build/${book} - ${author}--Quasebuda.pdf" \
	--resource-path=.:"${bookPath}/assets/":$resourcesPath \
        $copyrightPath $mdFiles \
	--toc --toc-depth=2 \
	-fmarkdown-implicit_figures \
	--include-in-header "${resourcesPath}/chapter-break.tex" \
	--include-before-body $coverPath \
	-V linkcolor:blue \
	-V geometry:a4paper \
	-V geometry:margin=2cm \
	-V mainfont="DejaVu Serif" \
	-V monofont="DejaVu Sans Mono" \
	--pdf-engine=xelatex \
	--lua-filter="${resourcesPath}/filter.lua"

rm -r .tmp
